package com.samares_engineering.omf.omf_core_framework.errors.exceptions.core;

public class OMFRollBackException extends RuntimeException{
    public OMFRollBackException(String message) {
        super(message);
    }
}
