package com.samares_engineering.omf.omf_core_framework.feature.registrables;

import com.samares_engineering.omf.omf_core_framework.feature.RegistrableFeatureItem;

public interface OnMagicDrawStart extends RegistrableFeatureItem {

    Runnable getOnMagicDrawStartRunnable();
}
