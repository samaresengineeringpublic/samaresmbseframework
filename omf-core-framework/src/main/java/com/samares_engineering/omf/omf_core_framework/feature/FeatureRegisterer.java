/*******************************************************************************
 * @copyright Copyright (c) 2022-2023 Samares-Engineering
 * @Licence: EPL 2.0
 * @Author: Quentin Cespédès, Clément Mezerette, Hugo Stinson
 * @since 0.0.0
 ******************************************************************************/

package com.samares_engineering.omf.omf_core_framework.feature;

import com.samares_engineering.omf.omf_core_framework.errors.OMFErrorHandler;
import com.samares_engineering.omf.omf_core_framework.errors.exceptions.feature.OMFFeatureRegisteringException;
import com.samares_engineering.omf.omf_core_framework.errors.exceptions.feature.OMFFrameworkException;
import com.samares_engineering.omf.omf_core_framework.errors.exceptions.general.GenericException;
import com.samares_engineering.omf.omf_core_framework.feature.registrables.itemregisterer.FeatureItemRegisterer;
import com.samares_engineering.omf.omf_core_framework.feature.registrables.itemregisterer.ProjectOnlyFeatureItemRegisterer;
import com.samares_engineering.omf.omf_core_framework.feature.registrables.listener.FeatureRegisteringEventHandler;
import com.samares_engineering.omf.omf_core_framework.plugin.APlugin;
import com.samares_engineering.omf.omf_core_framework.utils.OMFUtils;

import java.util.ArrayList;
import java.util.List;

public class FeatureRegisterer {
    private List<FeatureItemRegisterer> featureItemRegisters;
    private List<ProjectOnlyFeatureItemRegisterer> projectOnlyFeatureItemRegisters;

    private final FeatureRegisteringEventHandler eventHandler;

    private List<MDFeature> registeredFeatures = new ArrayList<>();
    private final APlugin plugin;

    //TODO: Create a class regrouping all Configurators
    public FeatureRegisterer(APlugin plugin) {
        this.plugin = plugin;
        this.featureItemRegisters = new ArrayList<>();
        this.projectOnlyFeatureItemRegisters = new ArrayList<>();
        this.eventHandler = new FeatureRegisteringEventHandler(this);
    }

    /**
     * Register a feature using delegation to register all its items using the according item registerer
     * ProjectOnly items are registered only if the project is opened
     *
     * @param feature the feature to register
     */
    public void registerFeature(MDFeature feature) {
        try {
            if (isAlreadyRegistered(feature)) {
                OMFErrorHandler.handleException(new OMFFrameworkException("Trying to register feature " + feature.getName() +
                        " which is already registered.", GenericException.ECriticality.ALERT));
            }
            feature.initFeature(plugin);
            feature.register();

            registerFeatureItems(feature);
            if (OMFUtils.getProject() != null) {
                registerProjectOnlyFeatureItems(feature);
            }
            registeredFeatures.add(feature);
            eventHandler.fireFeatureRegistered(feature);
        } catch (Exception e) {
            OMFErrorHandler.handleException(new OMFFrameworkException("Error while registering feature " + feature.getName(),
                    e, GenericException.ECriticality.CRITICAL), false);
        }
    }

    private void registerFeatureItems(MDFeature feature) {
        feature.initFeatureItems();
        for (FeatureItemRegisterer registerer : featureItemRegisters) {
            registerer.registerFeatureItems(feature);
        }
    }

    /**
     * Registers a list of features, see {@link FeatureRegisterer#registerFeature(MDFeature)}
     *
     * @param features the features to register
     */
    public void registerFeatures(List<MDFeature> features) {
        features.forEach(this::registerFeature);
    }

    /**
     * Registers project only items of a list of features, see {@link FeatureRegisterer#registerProjectOnlyFeatureItems(MDFeature)}
     *
     * @param features the features to register
     */
    public void registerProjectOnlyItemsOfFeatures(List<MDFeature> features) {
        features.forEach(this::registerProjectOnlyFeatureItems);
    }

    /**
     * Registers feature items that are declared as "project only" until project is opened. This method is then called
     * every time the project opens.
     * On the first registration, the items are also initialised. We wait until the project to be opened to initialise
     * the items in order to avoid instances where the items need the project to be opened to function, for example if
     * you need to set a default value from the Sysml profile in an Option
     *
     * @param feature the feature to register
     */
    private void registerProjectOnlyFeatureItems(MDFeature feature) {
        feature.initProjectOnlyFeatureItems();
        projectOnlyFeatureItemRegisters.forEach(registerer -> {
            try {
                registerer.registerFeatureItems(feature);
            } catch (Exception e) {
                OMFErrorHandler.handleException(
                        new OMFFrameworkException("Error while registering project only items for feature " +
                                feature.getName(), e, GenericException.ECriticality.CRITICAL));
            }
        });

    }

    /**
     * Unregisters a feature using delegation to unregister all its items using the according item registerer
     * On Failure it will continue to unregister the feature items and then throw an exception for each item that failed
     *
     * @param feature the feature to unregister
     */
    public void unregisterFeature(MDFeature feature) throws OMFFeatureRegisteringException {
        if (!isAlreadyRegistered(feature)) {
            OMFErrorHandler.handleException(new OMFFrameworkException("Trying to unregister feature "
                    + feature.getName() + " which is not registered.", GenericException.ECriticality.ALERT));
        }

        registeredFeatures.remove(feature);
        for (FeatureItemRegisterer<?> registerer : featureItemRegisters) {
            try {
                registerer.unregisterFeatureItems(feature);
            } catch (Exception e) {
                throw new OMFFeatureRegisteringException(
                        "Error while unregistering items for feature " + feature.getName(), e);
            }
        }

        for (FeatureItemRegisterer<?> registerer : projectOnlyFeatureItemRegisters) {
            try {
                registerer.unregisterFeatureItems(feature);
            } catch (Exception e) {
                throw new OMFFeatureRegisteringException("Error while unregistering Project only items for feature " +
                        feature.getName(), e);
            }
        }

        feature.unregister();
        eventHandler.fireFeatureUnregistered(feature);
    }

    /**
     * Unregisters a list of features, see {@link FeatureRegisterer#unregisterFeature(MDFeature)}
     *
     * @param features the features to unregister
     */
    public void unregisterFeatures(List<MDFeature> features) {
        new ArrayList<>(features).forEach(feature -> {
            try {
                unregisterFeature(feature);
            } catch (Exception e) {
                OMFErrorHandler.handleException(new OMFFrameworkException("Error while unregistering feature " +
                        feature.getName(), e, GenericException.ECriticality.CRITICAL), false);
            }
        });
    }

    /**
     * Unregisters project only items of a list of features, see {@link FeatureRegisterer#unregisterProjectOnlyFeatureItems(MDFeature)}
     *
     * @param features the features to unregister
     */
    public void unregisterProjectOnlyItemsOfFeatures(List<MDFeature> features) {
        new ArrayList<>(features).forEach(this::unregisterProjectOnlyFeatureItems);//New Arraylist to manage List modifications while iterating
    }

    /**
     * Unregisters project only items of a feature using delegation to unregister all its items using the according item registerer
     *
     * @param feature the feature to unregister
     */
    public void unregisterProjectOnlyFeatureItems(MDFeature feature) {
        projectOnlyFeatureItemRegisters.forEach(registerer -> {
            try {
                registerer.unregisterFeatureItems(feature);
            } catch (Exception e) {
                OMFErrorHandler.handleException(new OMFFrameworkException("Error while unregistering project only items for" +
                        " feature " + feature.getName(), e, GenericException.ECriticality.CRITICAL), false);
            }
        });
    }

    /**
     * Checks if a feature is already registered
     */
    public boolean isAlreadyRegistered(MDFeature mdFeature) {
        return registeredFeatures.stream().anyMatch(mdFeature.getClass()::isInstance);
    }

    /**
     * Adds a feature item registerer to the list of item registerers.
     * The registerer is initialised with the current instance of the FeatureRegisterer
     * FeatureItemRegisterer are used to register/unregister feature items of a feature
     * and will be called when a feature is registered/unregistered
     */
    public void addIFeatureItemRegisterer(FeatureItemRegisterer featureItemRegisterer) {
        try {
            this.featureItemRegisters.add(featureItemRegisterer);
            featureItemRegisterer.init(this);
        } catch (Exception e) {
            OMFErrorHandler.handleException(new OMFFrameworkException("Error while adding feature item registerer " +
                    featureItemRegisterer.getClass().getName(), e, GenericException.ECriticality.CRITICAL), false);
        }
    }

    /**
     * Adds a list of feature item registerers to the list of item registerers,
     * see {@link FeatureRegisterer#addIFeatureItemRegisterer(FeatureItemRegisterer)}
     */
    public void addAllIFeatureItemRegisterer(List<? extends FeatureItemRegisterer> featureItemRegisterers) {
        featureItemRegisterers.forEach(this::addIFeatureItemRegisterer);
    }

    /**
     * Removes a feature item registerer from the list of item registerers.
     * The registerer is initialised with the current instance of the FeatureRegisterer
     * FeatureItemRegisterer are used to register/unregister feature items of a feature
     * and will be called when a feature is registered/unregistered
     */
    public void removeIFeatureItemRegisterer(FeatureItemRegisterer featureItemRegisterer) {
        this.featureItemRegisters.remove(featureItemRegisterer);
    }

    /**
     * Removes a list of feature item registerers from the list of item registerers,
     * see {@link FeatureRegisterer#removeIFeatureItemRegisterer(FeatureItemRegisterer)}
     */
    public void removeAllIFeatureItemRegisterer(List<? extends FeatureItemRegisterer> featureItemRegisterers) {
        featureItemRegisterers.forEach(this::removeIFeatureItemRegisterer);
    }

    /**
     * Adds a project only feature item registerer to the list of item registerers.
     * The registerer is initialised with the current instance of the FeatureRegisterer
     * ProjectOnlyFeatureItemRegisterer are used to register/unregister project only feature items of a feature
     * and will be called when a feature is registered/unregistered
     */
    public void addProjectOnlyFeatureItemRegisterer(ProjectOnlyFeatureItemRegisterer featureItemRegisterer) {
        try {
            this.projectOnlyFeatureItemRegisters.add(featureItemRegisterer);
            featureItemRegisterer.init(this);
        } catch (Exception e) {
            OMFErrorHandler.handleException(new OMFFrameworkException("Error while adding project only feature item registerer " +
                    featureItemRegisterer.getClass().getName(), e, GenericException.ECriticality.CRITICAL), false);
        }
    }

    /**
     * Adds a list of project only feature item registerers to the list of item registerers,
     * see {@link FeatureRegisterer#addProjectOnlyFeatureItemRegisterer(ProjectOnlyFeatureItemRegisterer)}
     */
    public void addAllProjectOnlyFeatureItemRegisterer(List<? extends ProjectOnlyFeatureItemRegisterer> featureItemRegisterers) {
        featureItemRegisterers.forEach(this::addProjectOnlyFeatureItemRegisterer);
    }

    /**
     * Removes a project only feature item registerer from the list of item registerers.
     * The registerer is initialised with the current instance of the FeatureRegisterer
     * ProjectOnlyFeatureItemRegisterer are used to register/unregister project only feature items of a feature
     * and will be called when a feature is registered/unregistered
     */
    public void removeProjectOnlyFeatureItemRegisterer(ProjectOnlyFeatureItemRegisterer featureItemRegisterer) {
        this.projectOnlyFeatureItemRegisters.remove(featureItemRegisterer);
    }

    /**
     * Removes a list of project only feature item registerers from the list of item registerers,
     * see {@link FeatureRegisterer#removeProjectOnlyFeatureItemRegisterer(ProjectOnlyFeatureItemRegisterer)}
     */
    public void removeAllProjectOnlyFeatureItemRegisterer(List<? extends ProjectOnlyFeatureItemRegisterer> featureItemRegisterers) {
        featureItemRegisterers.forEach(this::removeProjectOnlyFeatureItemRegisterer);
    }

    //-------------------------------- GETTER / SETTER --------------------------------------------

    /**
     * Returns all registered features
     */
    public List<MDFeature> getRegisteredFeatures() {
        return registeredFeatures;
    }

    public void setRegisteredFeatures(List<MDFeature> registeredFeatures) {
        this.registeredFeatures = registeredFeatures;
    }

    /**
     * Get the plugin instance
     */
    public APlugin getPlugin() {
        return plugin;
    }

    public FeatureRegisteringEventHandler getEventHandler() {
        return eventHandler;
    }
}
